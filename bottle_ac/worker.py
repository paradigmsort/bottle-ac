from aiohttp.worker import AsyncGunicornWorker
from aiohttp.wsgi import WSGIServerHttpProtocol


class FixedGunicornWorker(AsyncGunicornWorker):
    def factory(self, wsgi, host, port):
        proto = WSGIServerHttpProtocol(
            wsgi, loop=self.loop,
            log=self.log,
            readpayload=True,
            access_log=self.log.access_log,
            access_log_format=self.cfg.access_log_format)
        return self.wrap_protocol(proto)